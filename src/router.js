import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: `/`,
      name: 'home',
      component: Home
    },
    {
      path: `/movie/:id`,
      name: 'profile',
      component: () => import('./pages/profile')
    },
    {
      path: `/favourites`,
      name: 'favourites',
      component: () => import('./pages/favourites')
    },
    {
      path: '*',
      redirect: {
        name: 'home'
      }
    }
  ]
})
