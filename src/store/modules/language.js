export default {
  namespaced: true,

  state: {
    lang: 'en'
  },
  getters: {
    getLanguage: state => state.lang
  },
  mutations: {
    SET_LANGUAGE(state, lang) {
      state.lang = lang
    }
  },
  actions: {
    setLanguage({ commit }, language) {
      commit('SET_LANGUAGE', language)
    }
  }
}
