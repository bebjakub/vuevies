export default {
  namespaced: true,

  state: {
    text: ''
  },
  getters: {
    getFilter: state => state.text,
  },
  mutations: {
    SET_FILTER(state, text) {
      state.text = text
    }
  },
  actions: {
    setFilter({ commit }, text) {
      commit('SET_FILTER', text)
    }
  }
}
