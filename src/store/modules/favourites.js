export default {
  namespaced: true,

  state: {
    movies: []
  },
  getters: {
    getFavourites: state => state.movies
  },
  mutations: {
    SET_FAVOURITE(state, movie) {
      const exists = state.movies.find(i => i.imdbID === movie.imdbID)
      if (!exists) state.movies.unshift(movie)
    },
    UNSET_FAVOURITE(state, movieId) {
      state.movies = state.movies.filter(movie => movie.imdbID !== movieId)
    }
  },
  actions: {
    addFavourite({ commit }, movie) {
      commit('SET_FAVOURITE', movie)
    },
    removeFavourite({ commit }, movieId) {
      commit('UNSET_FAVOURITE', movieId)
    }
  }
}
