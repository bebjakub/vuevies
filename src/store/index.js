import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import language from './modules/language'
import filter from './modules/filter'
import favourites from './modules/favourites'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    language,
    filter,
    favourites
  },
  plugins: [
    new VuexPersistence({
      modules: [
        'language',
        'favourites'
      ]
    }).plugin
  ]
})
