import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'

import i18n from './i18n'
import InfiniteLoading from 'vue-infinite-loading'

import '@/assets/scss/main.scss'

import { LayoutPlugin, CardPlugin } from 'bootstrap-vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.use(InfiniteLoading, {
  slots: {
    noMore: '',
    noResults: ''
  }
})

Vue.use(LayoutPlugin)
Vue.use(CardPlugin)

library.add(faStar)
Vue.component('font-awesome-icon', FontAwesomeIcon)


Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount('#app')
